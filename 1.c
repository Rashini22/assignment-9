/*IS1101 Programming Assignment 9
Name:P.Rashini Lakshika
Index No:20020619*/

#include<stdio.h>

int main()
{
    FILE *myFile;

        char sentence1[100];
        //Write the first sentence into the file
        myFile=fopen("assignment9.txt","w");//Write mode
            fprintf(myFile,"UCSC is one of the leading institutes in Sri Lanka for computing studies.");
            fclose (myFile);

        //Read the first sentence from the file and print
        myFile=fopen("assignment9.txt","r");//Read mode
            while(!feof(myFile))
                {
                    fgets(sentence1,100,myFile);
                    puts(sentence1);
                }
            fclose(myFile);

        // Append the second sentence into the file
        myFile=fopen("assignment9.txt","a");//Append mode
            fprintf(myFile,"\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
            fclose(myFile);

    return 0;
}
